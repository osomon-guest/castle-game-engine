#!/usr/bin/make -f
# debian/rules for Castle Game Engine

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk
export DH_ALWAYS_EXCLUDE := COPYING:LICENSE
export LANG:=C

# Documentation type to use chm/html
ifeq (${DEBDOCTYPE},)
DOCTYPE=html
# HTML Converter chmdocs or htmldocs for (default)
CONVERTER=${DOCTYPE}docs
else
DOCTYPE=${DEBDOCTYPE}
CONVERTER=${DOCTYPE}docs
endif
# Define FPC
ifndef FPC
FPC=/usr/bin/fpc
endif
# Set FPCVER
FPCVER=$(shell ${FPC} -iV)

CPU_TARGET?=$(shell ${FPC} -iTP)
OS_TARGET?=$(shell ${FPC} -iTO)
FPCTARGET=${CPU_TARGET}-${OS_TARGET}
# Used in dh-exec enhanced files
export CGE_VERSION=$(shell echo ${DEB_VERSION_UPSTREAM} | sed -e 's/^\([0-9\.]*\).*/\1/')
DEB_BUILD=$(lastword $(subst -, ,${DEB_VERSION}))
ifndef PACKAGESUFFIX
PACKAGESUFFIX=-${CGE_VERSION}
endif
# Get directories
INSTALL_DIR=$(CURDIR)/debian/tmp
LIB_DIR=${INSTALL_DIR}/usr/lib/${DEB_SOURCE}/${CGE_VERSION}
SRC_DIR=$(CURDIR)/debian/src/${DEB_SOURCE}-${CGE_VERSION}
UNITS_DIR=${INSTALL_DIR}/usr/lib/${DEB_HOST_MULTIARCH}/fp-units-${FPCVER}/${DEB_SOURCE}-${CGE_VERSION}
TMP_DIR=${INSTALL_DIR}/tmp
# Get utils
ifndef MKDIR
MKDIR=mkdir -p
endif
ifndef CP
CP=cp -Rfpl
endif
RM:=rm -rf

#export DH_VERBOSE=1

%:
	dh ${@}

override_dh_auto_clean:
	${MAKE} cleanall
	$(MAKE) strip-precompiled-libraries
	dh_auto_clean

prepare-source: prepare-source-stamp
prepare-source-stamp:
	# Building Pascal style font file first while making sure that
	# no unit or object files polute the source tree
	${MKDIR} ${TMP_DIR}
	${FPC} -FE${TMP_DIR}  @castle-fpc.cfg tools/texture-font-to-pascal/texture-font-to-pascal.lpr
	cd src/fonts ; PATH=${PATH}:${TMP_DIR} ./mk_fonts.sh

	# cge embeds some images from xcf via png into Pascal source files
	# First we need to create the pngs
	for pngfile in $$(grep \\\.png\$$ debian/clean) ; do xcf2png $${pngfile%.png}.xcf -o $${pngfile} ; done
	# Then we build the helper function
	${FPC} -FE${TMP_DIR} -dRELEASE @castle-fpc.cfg tools/image-to-pascal/image-to-pascal.lpr
	# To finally convert png files to pas file
	PATH=${TMP_DIR} ${MAKE} -C src/ui/opengl/gui-images all

	# Upstream source contains auto-generated include files. Let's generate
	# them again.
	${FPC} -FE${TMP_DIR} -dRELEASE @castle-fpc.cfg src/x3d/nodes_specification/x3d-nodes-to-pascal/x3d-nodes-to-pascal.lpr
	cd src/x3d/nodes_specification/x3d-nodes-to-pascal ; \
		PATH=${TMP_DIR} x3d-nodes-to-pascal ../components/*.txt

	# For reproducible builds we need to make sure the timestamp of the just
	# created files is fixed. Let's use SOURCE_DATE_EPOCH for that. For the
	# future, this needs to be even smarter, because now every Debian revision
	# will require all the reverse dependencies generating ppu files to be
	# rebuild.
	touch --no-dereference --date="@$${SOURCE_DATE_EPOCH}" \
		src/x3d/auto_generated_node_helpers/*.inc \
		src/fonts/castletexturefont_*.pas \
		src/ui/opengl/castlecontrolsimages.*

	touch prepare-source-stamp

# Annoying thing is we need to do this very early to save lots of overhead. If
# it can be done on a clean tree, right after prepare-source we don't need
# cleaning, right.
install-source: install-source-stamp
install-source-stamp:
	@echo "--- Copying the source code"
	dh_testdir
	${MKDIR} ${SRC_DIR}
	${CP} -t ${SRC_DIR} \
		$(CURDIR)/src/*
	find $(SRC_DIR) -name '*.bmp' -exec chmod 644 '{}' ';'
	find ${SRC_DIR} -empty -delete
	touch install-source-stamp

override_dh_auto_build-arch: prepare-source install-source
	# Build the real engine
	${MAKE} all
	# Generate man pages
	${MAKE} -C doc/man/man1

override_dh_auto_test-arch:
	# Command basically copied from tests/compile_console.sh while
	# making sure that no unit or object files polute the source tree
	${FPC}  -gh -dDEBUG -dTEXT_RUNNER -dNO_WINDOW_SYSTEM \
		-Fuexamples/research_special_rendering_methods/shadow_fields/ \
		-Fuexamples/curves_surfaces/ \
		-FE${TMP_DIR} \
		@castle-fpc.cfg tests/test_castle_game_engine.lpr
	# cd is needed to find the data files
	cd tests ; ${TMP_DIR}/test_castle_game_engine -a

override_dh_auto_install-arch:
	# Install units
	${MKDIR} ${UNITS_DIR}
	${CP} -t ${UNITS_DIR} units/${FPCTARGET}/*
	# Mark package as manually compilable and install them
	for package in $$(find * -name '*.lpk') ; \
	do \
		pkgDir=$$(dirname "${LIB_DIR}/$${package}") ; \
		${MKDIR} "$${pkgDir}" ; \
		sed -e 's@\(\W*\)<CompilerOptions>.*@\1<AutoUpdate Value="Manually"/>\n&@' \
		    -e "s@\.\./src/@$$(relpath ${SRC_DIR} $${pkgDir})/@g" \
		    -e "s@\(\W*\)<UnitOutputDirectory .*@<UnitOutputDirectory Value=\"$$(relpath ${UNITS_DIR} $${pkgDir})\"/>@" \
		    "$${package}" > "${LIB_DIR}/$${package}" ; \
	done
	# Call upstream install target for the tools
	${MAKE} PREFIX=${INSTALL_DIR}/usr install
	find ${INSTALL_DIR} -empty -delete

override_dh_auto_build-indep: prepare-source install-source
	@echo "--- Building Documentation"
	# Create logo first, before generating documentation
	${MAKE} -C doc/pasdoc/logo
	${MAKE} -C doc/pasdoc ${DOCTYPE}
	# pasdoc create a jquery file, but we link from jquery instead
	${RM} doc/reference/tipuesearch/jquery.min.js

override_dh_auto_install-indep: install-source

override_dh_installdocs:
	dh_installdocs -X.in

override_dh_install:
	# tmp/tmp is used for the testsuite
	dh_install -Xtmp/tmp/

override_dh_compress-indep:
	dh_compress -X.pdf

override_dh_gencontrol:
	dh_gencontrol -- -V"dejavu:version=$$(dpkg-query --show fonts-dejavu | awk '{print $$2}')"

.PHONY:prepare-source install-source
