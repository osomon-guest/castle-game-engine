Source: castle-game-engine
Section: misc
Priority: optional
Maintainer: Pascal Packaging Team <pkg-pascal-devel@lists.alioth.debian.org>
Uploaders:
 Abou Al Montacir <abou.almontacir@sfr.fr>,
 Paul Gevers <elbrus@debian.org>
Standards-Version: 4.5.0
Build-Depends:
 debhelper-compat (= 13),
 dh-exec,
 fonts-dejavu,
 fp-compiler,
 fp-units-base,
 fp-units-db,
 fp-units-fcl,
 fp-units-gfx,
 fp-units-gtk2,
 fp-units-misc,
 fp-units-multimedia,
 fp-units-net,
 fp-utils (>= 2.6.2),
 help2man,
 libatk1.0-dev,
 libcairo2-dev,
 libfreetype6-dev,
 libgdk-pixbuf2.0-dev,
 libgtk2.0-dev,
 libgtkglext1-dev,
 libpango1.0-dev,
 xcftools,
Build-Depends-Indep:
 inkscape (>= 1.0~),
 pasdoc (>= 0.15.0~),
Vcs-Git: https://salsa.debian.org/pascal-team/castle-game-engine.git
Vcs-Browser: https://salsa.debian.org/pascal-team/castle-game-engine
Homepage: https://castle-engine.sourceforge.io/engine.php
Rules-Requires-Root: no

Package: fp-units-castle-game-engine
Architecture: any
Built-Using: fonts-dejavu (= ${dejavu:version})
Depends:
 ${fpc-abi:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 castle-game-engine-doc,
 fpc,
 lazarus
Suggests:
 castle-game-engine-src,
 gradle,
Description: Castle Game Engine - 3D game engine for FreePascal / Lazarus
 Castle Game Engine is a set of LGPL licenced libraries that are intended to
 ease developing 3D games with FreePascal / Lazarus.
 .
 It provides an excellent support for the VRML / X3D 3D data format. Other 3D
 formats are also supported.
 .
 It features many advanced graphic effects and easy to use API on top of OpenGL.
 .
 This package contains the FPC units and the auxiliary tools castle-curves,
 castle-engine, image-to-pascal, sprite-sheet-to-x3d, and
 texture-font-to-pascal.
 .
 This package suggests the gradle package. Gradle is only necessary if one
 wants to build Android applications, otherwise one can ignore that
 suggestion. Note that, in order to create Android applications, one will also
 need the Android SDK and NDK, and FPC cross-compiler, see
 https://github.com/castle-engine/castle-engine/wiki/Android.

Package: castle-game-engine-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 fonts-droid-fallback,
 ttf-dejavu-core,
Suggests:
 castle-game-engine-src,
 fp-units-castle-game-engine,
 libjs-jquery,
Description: Castle Game Engine - Developer's Documentation
 Castle Game Engine is a set of LGPL licenced libraries that are intended to
 ease developing 3D games with FreePascal / Lazarus.
 .
 It provides an excellent support for the VRML / X3D 3D data format. Other 3D
 formats are also supported.
 .
 It features many advanced graphic effects and easy to use API on top of OpenGL.
 .
 This package contains documentation.

Package: castle-game-engine-src
Section: doc
Architecture: all
Built-Using: fonts-dejavu (= ${dejavu:version})
Depends:
 fonts-dejavu,
 ${misc:Depends}
Recommends:
 castle-game-engine-doc,
 fpc,
 lazarus
Suggests:
 fp-units-castle-game-engine
Description: Castle Game Engine - Source code for Lazarus integration
 Castle Game Engine is a set of LGPL licenced libraries that are intended to
 ease developing 3D games with FreePascal / Lazarus.
 .
 It provides an excellent support for the VRML / X3D 3D data format. Other 3D
 formats are also supported.
 .
 It features many advanced graphic effects and easy to use API on top of OpenGL.
 .
 This package contains source code for integration with Lazarus RAD IDE.
